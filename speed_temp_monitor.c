#include <stdio.h>
#include <stdlib.h> //exit()
#include <fcntl.h> //define O_RDWR
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <unistd.h>
void main()
{
int file,i;
int addr = 0x2e; //i2c device address of gyro
char *filename = "/dev/i2c-1";
char wbuf1[] = {0x00, 0xb5}; //first byte is address to write. others are bytes to be written
char wbuf2[] = {0x01, 0x3f};
char wbuf3[] = {0x04, 0x48};
char read_start_buf[1];
float local_temp,remote_temp;
char local_temp_h[1],remote_temp_h[1];
char  temp_l[1];
int tach_count;
char tach_h[1],tach_l[1];
float rpm;

if((file = open(filename, O_RDWR)) < 0)
{
	perror("Failed to open i2c device.\n");
	exit(1);
}
if(ioctl(file, I2C_SLAVE, addr) < 0)
{
	printf("Failed to access bus.\n");
	exit(1);
}
write(file, wbuf1, 2); //write initial.
write(file, wbuf2, 2);
write(file, wbuf3, 2);
	read_start_buf[0] = 0x00;
	write(file, read_start_buf, 1);
	read(file, temp_l, 1);
	printf("%x\n",temp_l[0]);

	read_start_buf[0] = 0x01;
	write(file, read_start_buf, 1);
	read(file, temp_l, 1);
	printf("%x\n",temp_l[0]);

	read_start_buf[0] = 0x04;
	write(file, read_start_buf, 1);
	read(file, temp_l, 1);
	printf("%x\n",temp_l[0]);

usleep(500000);
while(1)
{
	int re;

	read_start_buf[0] = 0x06;
	re=write(file, read_start_buf, 1);
printf("%d\n",re);
	read(file, temp_l, 1);
printf("%x\n",temp_l[0]);

	read_start_buf[0] = 0x0a;
	write(file, read_start_buf, 1);
	read(file, local_temp_h, 1);
printf("%x\n",local_temp_h[0]);

	read_start_buf[0] = 0x0b;
	write(file, read_start_buf, 1);
	read(file, remote_temp_h, 1);
printf("%x\n",remote_temp_h[0]);	

	local_temp=(int)local_temp_h[0]+((temp_l[0]&0xe0)>>5)*0.125;
	remote_temp=(int)remote_temp_h[0]+(temp_l[0]&0x07)*0.125;
	
	printf("local_temp=%f\n",local_temp);
	printf("remote_temp=%f\n",remote_temp);

	read_start_buf[0] = 0x08;
	write(file, read_start_buf, 1);
	read(file, tach_l, 1);
printf("%x\n",tach_l[0]);

	read_start_buf[0] = 0x09;
	write(file, read_start_buf, 1);
	read(file, tach_h, 1);
printf("%x\n",tach_h[0]);

	tach_count=(int)tach_h[0]*256+(int)tach_l[0];
	printf("%d\n",tach_count);

	if(tach_count>0)
	{
		rpm=6000000.0/tach_count;
		printf("rpm=%f\n",rpm);
	}
	else 
	{
		printf("rpm=error\n");
	}
	sleep(1);


}
}

